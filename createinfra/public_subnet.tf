resource "aws_subnet" "public_subnet-A" {
  vpc_id             = aws_vpc.vpc.id
  availability_zone  = "${var.region}a"
  cidr_block         = var.public_subnet_1_cidr
  map_public_ip_on_launch = true
  tags = {
    Name = "${var.environment}_public-subnet-1"
  }
}


resource "aws_route_table_association" "add_route_A" {
  subnet_id      = aws_subnet.public_subnet-A.id
  route_table_id = aws_route_table.public_route.id
}


resource "aws_subnet" "public_subnet-B" {
  vpc_id             = aws_vpc.vpc.id
  availability_zone  = "${var.region}b"
  cidr_block         = var.public_subnet_2_cidr
  map_public_ip_on_launch = true
  tags = {
    Name = "${var.environment}_public-subnet-2"
  }
}

resource "aws_route_table_association" "add_route_B" {
  subnet_id      = aws_subnet.public_subnet-B.id
  route_table_id = aws_route_table.public_route.id
}
