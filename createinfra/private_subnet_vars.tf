variable private_subnet_1_cidr {
  type = "string"
  default = "10.0.3.0/24"
}

variable private_subnet_2_cidr  {
  type = "string"
  default = "10.0.4.0/24"
}
