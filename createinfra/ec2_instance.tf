
resource "aws_instance" "ec2" {
  ami           = var.ami
  instance_type = "t2.micro"
  subnet_id     = aws_subnet.public_subnet-A.id
  key_name = aws_key_pair.key_pair.key_name

  tags = {
       Name = var.ec2_name
  }
}

output "public_ip" {
  value = aws_instance.ec2.public_ip
}
