resource "aws_subnet" "private_subnet-A" {
  vpc_id             = aws_vpc.vpc.id
  availability_zone  = "${var.region}a"
  cidr_block         = var.private_subnet_1_cidr
  tags = {
    Name = "${var.environment}_private-subnet-1"
  }
}


resource "aws_route_table_association" "private_route_A" {
  subnet_id      = aws_subnet.private_subnet-A.id
  route_table_id = aws_route_table.private_route.id
}


resource "aws_subnet" "private_subnet-B" {
  vpc_id             = aws_vpc.vpc.id
  availability_zone  = "${var.region}b"
  cidr_block         = var.private_subnet_2_cidr
  tags = {
    Name = "${var.environment}_private-subnet-2"
  }
}

resource "aws_route_table_association" "private_route_B" {
  subnet_id      = aws_subnet.private_subnet-B.id
  route_table_id = aws_route_table.private_route.id
}
