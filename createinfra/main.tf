
resource "aws_vpc" "vpc" {
  cidr_block = var.cidr
  instance_tenancy = var.instance_tenancy
  tags =  {
    Name = "${var.environment}-vpc"
  }
}
resource "aws_internet_gateway" "igw" {
  vpc_id = aws_vpc.vpc.id
  tags =  {
    Name = "${var.environment}_igw"
  }
}

