variable "cidr" {
  type = "string"
  default = "10.0.0.0/16"
  description = "Ip address range"
}

variable "instance_tenancy" {
  type = "string"
  default = "default"
  description = "dedicated or default"
}


variable "environment" {
type = "string"
description = "Specify the environment"

}

variable "public_key_path" {
type = "string"
description = "Specify destination for private key"
default = "~/.ssh/id_rsa.pub"
}
variable "region" {
default   = "us-east-2"
type = "string"
}
